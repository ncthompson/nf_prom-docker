#!/bin/bash
mkdir -p go/bin
REPO=/go/src/github.com/hpdvanwyk
REPOFULL=$PWD/go/src/github.com/hpdvanwyk
export GOPATH="$PWD/go"
mkdir -p $REPOFULL
git clone https://github.com/hpdvanwyk/nf_prom.git "$REPOFULL/nf_prom"
echo "Clone complete"
docker run --rm -v $GOPATH:/go -w $REPO/nf_prom/ iron/go:dev make
echo "Build done"
docker build -t ncthompson/nf_prom -f dockerfile.scratch .
echo "Docker made"
