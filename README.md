# ThingsWeather-docker
The docker image builder repository for the [netflow monitor](https://github.com/hpdvanwyk/nf_prom).
## Usage

 Requirements:
 * dep - https://github.com/golang/dep
 * git
 
 Build:
 ```sh
 $ ./build.sh
 ```

 Run:
 ```sh
 $ docker-compose up -d
 ```
